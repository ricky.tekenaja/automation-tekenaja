from webbrowser import Chrome
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import pyautogui
import time


def SignerActivationFlow(driver,var0,passwordloginc):
    time.sleep(1)
    driver.find_element(By.ID, "current_password").send_keys(var0)
    driver.find_element(By.ID, "password").send_keys(passwordloginc)
    driver.find_element(By.ID, "password_confirmation").send_keys(passwordloginc)
    driver.find_element(By.ID, "btnSubmit").click()
    time.sleep(3)