from webbrowser import Chrome
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import pyautogui
import time


def RegisterFlow(driver,nik,nama,pob,dob,gender,phonenum,emailuser,zipcode,address,province,district,subdistrict,fotoktp,fotoselfie):
    time.sleep(1)
    driver.find_element(By.XPATH, '/html/body/section[2]/div/div/div[2]/center/a').send_keys(Keys.ENTER)
    time.sleep(1)
    driver.find_element(By.ID, "nik").send_keys(nik)
    driver.find_element(By.ID, "name").send_keys(nama)
    driver.find_element(By.ID, "pob").send_keys(pob)
    driver.find_element(By.ID, "dob").send_keys(dob)
    pyautogui.press("enter")

    Pilih = Select(driver.find_element(By.ID, "gender"))
    Pilih.select_by_value(gender)

    pyautogui.press("tab")
    pyautogui.typewrite(phonenum)

    driver.find_element(By.ID, "email").send_keys(emailuser)
    driver.find_element(By.ID, "confEmail").send_keys(emailuser)
    driver.find_element(By.ID, "address").send_keys(address)
    driver.find_element(By.ID, "zip_code").send_keys(zipcode)

    Pilih = Select(driver.find_element(By.ID, "provinceId"))
    Pilih.select_by_value(province)
    time.sleep(1)

    Pilih = Select(driver.find_element(By.ID, "districtId"))
    Pilih.select_by_value(district)
    time.sleep(1)

    Pilih = Select(driver.find_element(By.ID, "subDistrictId"))
    Pilih.select_by_value(subdistrict)
    time.sleep(1)

    driver.find_element(By.ID, "customFileKTP").send_keys(fotoktp)
    time.sleep(1)
    driver.find_element(By.ID, "customFileSelfie").send_keys(fotoselfie)
    time.sleep(1)

    driver.find_element(By.XPATH, '/html/body/section[2]/div[1]/div/div/form/div[9]/div/div/input').send_keys(" ")
    driver.find_element(By.XPATH, '/html/body/section[2]/div[1]/div/div/form/div[10]/div/center/button').send_keys(" ")
    time.sleep(1)
    driver.find_element(By.XPATH, '/html/body/div[3]/div/div[6]/button[1]').click()
    time.sleep(1)

    try:
        WebDriverWait(driver,1200).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[3]/div/div[6]/button[3]')))
        print("Register Berhasil")
        time.sleep(1)
        driver.find_element(By.XPATH, '/html/body/div[3]/div/div[6]/button[3]').click()

    except TimeoutException:
        print("gak berhasil")
        pass

    time.sleep(1)