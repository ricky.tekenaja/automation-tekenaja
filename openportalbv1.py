from webbrowser import Chrome
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
import pyautogui
import time

def BusinessLoginFlow(driver,https,b,domain,path15,emailloginb,passwordloginb):
    time.sleep(1)
    driver.maximize_window()
    driver.implicitly_wait(1)
    driver.get(https+b+domain+path15)
    time.sleep(1)
    driver.implicitly_wait(1)

    driver.find_element(By.ID, 'email').send_keys(emailloginb)
    driver.find_element(By.ID, "password").send_keys(passwordloginb)
    driver.find_element(By.CLASS_NAME, "btn.btn-primary").click()
    time.sleep(1)
    