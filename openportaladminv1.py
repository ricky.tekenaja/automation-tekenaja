from webbrowser import Chrome
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import random
import pyautogui
import pyperclip
import time

def OpenPortalAdminFLow (driver,https,admin,domain,path15,emailloginadmin,passloginadmin):
    driver.get(https+admin+domain+path15)
    driver.maximize_window()
    driver.implicitly_wait(2)
    driver.find_element(By.ID, "email").send_keys(emailloginadmin)
    time.sleep(1)
    driver.find_element(By.ID, "password").send_keys(passloginadmin)
    time.sleep(1)
    driver.find_element(By.XPATH, '/html/body/div[1]/main/div/div/div/div/div[2]/form/div[3]/div/button').click()
    time.sleep(1)