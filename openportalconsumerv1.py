from webbrowser import Chrome
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import pyautogui
import time


def ActivationSignerFlow(driver,https,c,domain,path15,emailuser,var0):
    time.sleep(1)
    driver.maximize_window()
    driver.get(https+c+domain+path15)
    time.sleep(1)
    driver.find_element(By.ID, "email").send_keys(emailuser)
    driver.find_element(By.ID, "password").send_keys(var0)
    driver.find_element(By.CLASS_NAME, "btn.btn-primary").click()
    time.sleep(2)
    

def SignerLoginFlow(driver,https,c,domain,path15,emailuser,passwordloginc):
    time.sleep(1)
    driver.get(https+c+domain+path15)
    time.sleep(1)
    driver.find_element(By.ID, 'email').send_keys(emailuser)
    driver.find_element(By.ID, "password").send_keys(passwordloginc)
    driver.find_element(By.CLASS_NAME, "btn.btn-primary").click()
    time.sleep(2)