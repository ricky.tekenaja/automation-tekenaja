from selenium import webdriver
import openportalbv1 as BusinessLoginFunc
import registerv1 as RegisterFunc
import clearemailv1 as ClearEmailFunc
import openportaladminv1 as OpenPortalAdminFunc
import openemailv1 as OpenEmailFunc
import getotpemailv1 as GetOtpEmailFunc
import uploadv1 as UploadFunc
import portalsignerconsumerv1 as ConsumerSigningFunc
import verifyemailv1 as VerifyEmailFunc
import openportalconsumerv1 as OpenPortalConsumerFunc
import activationsignerv1 as ActivationSignerFunc
import random
import string
import time

#URL APLIKASI WEBPORTAL
https = 'https://'
b,c,ttd,admin,cp,doc = 'b.','c.','ttd.','admin.','cp.','doc.'
domain = 'sandbox-111094.com' #SANDBOX
# domain = 'djelas.id' #PRODUCTION
path1 = '/documents'
path2 = '/documents/download-bulk'
path3 = '/documents/upload-tutorial'
path4 = '/documents/upload'
path5 = '/registrations'
path6 = '/registrations/form-link'
path7 = '/staffs'
path8 = '/staffs/create'
path9 = '/signers'
path10 = '/companies'
path11 = '/users/change-password'
path12 = '/roles'
path13 = '/activity'
path14 = '/logout'
path15 = '/login'
path16 = '/dashboard'
path17 = '/manage/user'
path18 = '/logout'

#EMAIL
yahoomail = 'https://mail.yahoo.com/'
ymail,ypass = "youremail@gmail.com","12345678"


emailloginb,passwordloginb = 'youremail@gmail.com','12345678' #SANDBOX PORTAL B
emailloginc,passwordloginc = 'youremail@gmail.com','12345678' #SANDBOX PORTAL C
emailloginadmin,passloginadmin = 'youremail@gmail.com','12345678' #SANDBOX PORTAL ADMIN

#SANDBOX
nik,nama,pob,dob,gender,phonenum,emailuser,zipcode,address,province,district,subdistrict,fotoktp,fotoselfie = '32711111111111','ayu','Jakarta','25/11/2000','1','85711111111','youremail@gmail.com','17111','Jl. Jalan','11','11','1','C:/Users/Yourname/Pictures/Camera Roll/foto.jpg','C:/Users/Yourname/Pictures/Camera Roll/ktp.jpeg'

digits = random.choices(string.digits, k=4)
letters = random.choices(string.ascii_uppercase, k=3)
joinletter = ''.join(letters)
joindigit = ''.join(digits)
randomid = (joinletter+joindigit)


option = webdriver.ChromeOptions()
option.add_experimental_option("excludeSwitches", ["enable-logging"])
driver = webdriver.Chrome(options=option)

driver.implicitly_wait(1)
time.sleep(1)

OpenPortalAdminFunc.OpenPortalAdminFLow(driver,https,admin,domain,path15,emailloginadmin,passloginadmin)
time.sleep(1)

ClearEmailFunc.ClearEmailFlow(driver,randomid,https,admin,domain,path17,emailuser)
time.sleep(1)

BusinessLoginFunc.BusinessLoginFlow(driver,https,b,domain,path15,emailloginb,passwordloginb)
time.sleep(1)

RegisterFunc.RegisterFlow(driver,nik,nama,pob,dob,gender,phonenum,emailuser,zipcode,address,province,district,subdistrict,fotoktp,fotoselfie)
time.sleep(1)

OpenEmailFunc.OpenEmailFlow(driver,ymail,ypass,yahoomail)
time.sleep(1)

VerifyEmailFunc.VerifyEmaiFlow(driver)
time.sleep(1)
var0 = VerifyEmailFunc.GetFreshPassword(driver)
time.sleep(1)

OpenPortalConsumerFunc.ActivationSignerFlow(driver,https,c,domain,path15,emailuser,var0)
time.sleep(1)

ActivationSignerFunc.SignerActivationFlow(driver,var0,passwordloginc)
time.sleep(1)

UploadFunc.UploadFlow(driver,https,b,domain,emailuser,path4)
time.sleep(1)
var1 = UploadFunc.GetDocID(driver)
time.sleep(1)

GetOtpEmailFunc.GetOtpEmailFlow(driver,yahoomail,ymail,ypass)
time.sleep(1)
var2 = GetOtpEmailFunc.CopyPinOTP(driver)
time.sleep(1)

ConsumerSigningFunc.PortalCosumerFlow(driver,https,c,domain,path15,emailuser,ypass,var1,var2)
time.sleep(1)

driver.quit()