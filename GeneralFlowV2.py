from selenium import webdriver
from decouple import config
import openportalbv1 as BusinessLoginFunc
import registerv1 as RegisterFunc
import clearemailv1 as ClearEmailFunc
import openportaladminv1 as OpenPortalAdminFunc
import openemailv1 as OpenEmailFunc
import getotpemailv1 as GetOtpEmailFunc
import uploadv1 as UploadFunc
import portalsignerconsumerv1 as ConsumerSigningFunc
import verifyemailv1 as VerifyEmailFunc
import openportalconsumerv1 as OpenPortalConsumerFunc
import activationsignerv1 as ActivationSignerFunc
import random
import string
import time

#APPLICATION URL
https = config('HTTPS')

domain = config('SANDBOX_DOMAIN') #SANDBOX
# domain = config('PROD_DOMAIN') #PRODUCTION

b = config('BUSINESS')
c = config('CONSUMER')
ttd = config('SIGNING')
admin = config('SANDBOX_PANEL')
cp = config('PROD_PANEL')
doc = config('DOCUMENT')

path1 = config('PATH1')
path2 = config('PATH2')
path3 = config('PATH3')
path4 = config('PATH4')
path5 = config('PATH5')
path6 = config('PATH6')
path7 = config('PATH7')
path8 = config('PATH8')
path9 = config('PATH9')
path10 = config('PATH10')
path11 = config('PATH11')
path12 = config('PATH12')
path13 = config('PATH13')
path14 = config('PATH14')
path15 = config('PATH15')
path16 = config('PATH16')
path17 = config('PATH17')
path18 = config('PATH18')

#EMAIL
yahoomail = config('YAHOO_MAIL')
ymail,ypass = config('YAHOO_USER_EMAIL'),config('YAHOO_USER_PWD')


emailloginc,passwordloginc = config('SANDBOX_C_EMAIL_LOGIN'),config('SANDBOX_C_PWD_LOGIN')
# emailloginc,passwordloginc = config('PROD_C_EMAIL_LOGIN'),config('PROD_C_PWD_LOGIN')
emailloginb,passwordloginb = config('SANDBOX_B_EMAIL_LOGIN'),config('SANDBOX_B_PWD_LOGIN')
# emailloginb,passwordloginb = config('PROD_B_EMAIL_LOGIN'),config('PROD_B_PWD_LOGIN')
emailloginadmin,passloginadmin = config('SANDBOX_CP_EMAIL_LOGIN'),config('SANDBOX_CP_PWD_LOGIN')
# emailloginadmin,passloginadmin = config('PROD_CP_EMAIL_LOGIN'),config('PROD_CP_PWD_LOGIN')


#SANDBOX
nik,nama,pob,dob,gender,phonenum,emailuser,zipcode,address,province,district,subdistrict,fotoktp,fotoselfie = config('NIK_USER'),config('NAME_USER'),config('POB_USER'),config('DOB_USER'),config('GENDER_USER'),config('PHONE_NUM_USER'),config('EMAIL_SIGNER_USER_SANDBOX'),config('ZIPCODE_USER'),config('ADDRESS_USER'),config('PROVINCE_CODE'),config('DISTRICT_CODE'),config('SUBDISTRICT_CODE'),config('KTP_FILE_PATH'),config('SELFIE_FILE_PATH')
#PRODUCTION
# nik,nama,pob,dob,gender,phonenum,emailuser,zipcode,address,province,district,subdistrict,fotoktp,fotoselfie = config('NIK_USER'),config('NAME_USER'),config('POB_USER'),config('DOB_USER'),config('GENDER_USER'),config('PHONE_NUM_USER'),config('EMAIL_SIGNER_USER_PROD'),config('ZIPCODE_USER'),config('ADDRESS_USER'),config('PROVINCE_CODE'),config('DISTRICT_CODE'),config('SUBDISTRICT_CODE'),config('KTP_FILE_PATH'),config('SELFIE_FILE_PATH')


digits = random.choices(string.digits, k=4)
letters = random.choices(string.ascii_uppercase, k=3)
joinletter = ''.join(letters)
joindigit = ''.join(digits)
randomid = (joinletter+joindigit)


option = webdriver.ChromeOptions()
option.add_experimental_option("excludeSwitches", ["enable-logging"])
option.add_experimental_option("detach", True) #Disable Auto Quit
option.add_argument("--disable-infobars")
option.add_argument("start-maximized")
option.add_argument("--disable-popup-blocking")
option.add_argument("--disable-extensions")
# Pass the argument 1 to allow and 2 to block
option.add_experimental_option("prefs", { \
    "profile.default_content_setting_values.media_stream_mic": 0, 
    "profile.default_content_setting_values.media_stream_camera": 1,
    "profile.default_content_setting_values.geolocation": 0, 
    "profile.default_content_setting_values.notifications": 0 
  })
driver = webdriver.Chrome(options=option)

time.sleep(1)

OpenPortalAdminFunc.OpenPortalAdminFLow(driver,https,admin,domain,path15,emailloginadmin,passloginadmin)
time.sleep(1)

ClearEmailFunc.ClearEmailFlow(driver,randomid,https,admin,domain,path17,emailuser)
time.sleep(1)

BusinessLoginFunc.BusinessLoginFlow(driver,https,b,domain,path15,emailloginb,passwordloginb)
time.sleep(1)

RegisterFunc.RegisterFlow(driver,nik,nama,pob,dob,gender,phonenum,emailuser,zipcode,address,province,district,subdistrict,fotoktp,fotoselfie)
time.sleep(1)

OpenEmailFunc.OpenEmailFlow(driver,ymail,ypass,yahoomail)
time.sleep(1)

VerifyEmailFunc.VerifyEmaiFlow(driver)
time.sleep(1)
var0 = VerifyEmailFunc.GetFreshPassword(driver)
time.sleep(1)

OpenPortalConsumerFunc.ActivationSignerFlow(driver,https,c,domain,path15,emailuser,var0)
time.sleep(1)

ActivationSignerFunc.SignerActivationFlow(driver,var0,passwordloginc)
time.sleep(1)

UploadFunc.UploadFlow(driver,https,b,domain,emailuser,path4)
time.sleep(1)
var1 = UploadFunc.GetDocID(driver)
time.sleep(1)

GetOtpEmailFunc.GetOtpEmailFlow(driver,yahoomail,ymail,ypass)
time.sleep(1)
var2 = GetOtpEmailFunc.CopyPinOTP(driver)
time.sleep(1)

ConsumerSigningFunc.PortalCosumerFlow(driver,https,c,domain,path15,emailuser,ypass,var1,var2)
time.sleep(1)

driver.quit()