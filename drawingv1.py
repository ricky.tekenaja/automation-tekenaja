def DrawingFlow():
    import pyautogui
    pyautogui.FAILSAFE = False

    distance = 200

    while distance > 0:
        pyautogui.dragRel(distance, 0, button='left', duration=0.1)
        distance -= 20
        pyautogui.dragRel(0, distance, button='left', duration=0.1)
        pyautogui.dragRel(-distance, 0, button='left', duration=0.1)
        distance -= 20
        pyautogui.dragRel(0, -distance, button='left', duration=0.1)
        