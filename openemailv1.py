from webbrowser import Chrome
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

def OpenEmailFlow (driver,ymail,ypass,yahoomail):
    driver.get(yahoomail)
    driver.implicitly_wait(1)
    driver.maximize_window()
    time.sleep(1)
    driver.find_element(By.XPATH, '/html/body/div[1]/a').click()
    time.sleep(1)
    driver.find_element(By.ID, "login-username").send_keys(f'{ymail}\n')
    time.sleep(1)
    driver.find_element(By.ID, "login-passwd").send_keys(f'{ypass}\n')
    time.sleep(2)
