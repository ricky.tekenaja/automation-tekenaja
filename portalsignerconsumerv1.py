from webbrowser import Chrome
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import pyautogui
import time

def PortalCosumerFlow(driver,https,c,domain,path15,emailuser,ypass,var1,var2):
    import drawingv1 as DrawingFunc

    time.sleep(1)
    driver.get(https+c+domain+path15)
    driver.find_element(By.ID, 'email').send_keys(emailuser)
    driver.find_element(By.ID, "password").send_keys(ypass)
    time.sleep(1)
    driver.find_element(By.CLASS_NAME, "btn.btn-primary").click()
    time.sleep(1)
    
    ####SIGNING PROSES#####
    driver.find_element(By.PARTIAL_LINK_TEXT, var1).send_keys(Keys.ENTER)
    print("masuk kehalaman signing")
    time.sleep(1)

    driver.find_element(By.ID, 'btnDocPreview').click()
    time.sleep(2)

    try:
        WebDriverWait(driver,1200).until(EC.visibility_of_element_located((By.ID, "exampleModalLabel")))
        print("document Muncul")
        pyautogui.PAUSE = 1
        pyautogui.moveTo(950,600, duration=0.5)
        pyautogui.scroll(600)
        pyautogui.PAUSE = 1
        pyautogui.scroll(-600)
        time.sleep(1)

    except TimeoutException:
        print("preview document gak muncul")
        pass



    driver.find_element(By.CLASS_NAME, 'close').click()
    time.sleep(1)

    driver.find_element(By.XPATH, '/html/body/div[1]/main/div/div[1]/form/div/div[1]/center/button').click()
    time.sleep(1)
    
    ####DRAWING#####
    pyautogui.moveTo(1300,500, duration=0.3)
    DrawingFunc.DrawingFlow()
    time.sleep(1)    
    
    driver.find_element(By.XPATH, '/html/body/div[1]/main/div/div[1]/form/div/div[2]/div[4]/div[2]/button').click()
    time.sleep(1)
    pyautogui.write(var2)
    driver.find_element(By.ID, 'buttonSubmit').click()
    time.sleep(1)
    print(driver.current_url+" - "+driver.title)
    time.sleep(1)