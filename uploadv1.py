from webbrowser import Chrome
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import pyautogui
import time

def UploadFlow(driver,https,b,domain,emailuser,path4):
    time.sleep(1)
    driver.implicitly_wait(10)
    driver.get(https+b+domain+path4)
    time.sleep(1)

    try:
        WebDriverWait(driver,2).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div/div[6]/button[1]')))
        time.sleep(1)
        driver.find_element(By.XPATH, '/html/body/div[2]/div/div[6]/button[1]').click()
        print("Token Mau Habis")

    except TimeoutException:
        print("Token Gak Habis")
        pass
    
    time.sleep(1)
    driver.implicitly_wait(1)
    driver.find_element(By.ID, "fileUploadForm").click()
    time.sleep(1)

    
    ##DOCUMENT SIZE CHOOSE##
    pyautogui.write(r"C:\Users\DTB053\Documents\Testing\test.pdf")
    # pyautogui.write(r"C:\Users\DTB053\Documents\Testing\7mb.pdf")
    # pyautogui.write(document1)
    # pyautogui.write(r"C:\Users\DTB053\Documents\Testing\31mb.pdf")


    time.sleep(1)
    pyautogui.press("enter")
    print("File Berhasil di Pilih")
    time.sleep(1)

    try:
        WebDriverWait(driver,1200).until(EC.visibility_of_element_located((By.ID, "swal2-title")))
        time.sleep(1)
        driver.find_element(By.CLASS_NAME, "swal2-actions").click()

    except TimeoutException:
        print("gak berhasil")
        pass
    
    time.sleep(1)
    
    # driver.find_element(By.XPATH, "/html/body/div[2]/div/div[6]/button[1]").click()
    # time.sleep(1)
    driver.find_element(By.ID, "pdfViewer").click()
    time.sleep(1)
    driver.find_element(By.ID, "gen_box_1_email").send_keys(emailuser)
    time.sleep(1)
    pyautogui.press("enter")
    time.sleep(1)
    driver.find_element(By.ID, "submit").click()
    time.sleep(1)
    driver.find_element(By.XPATH, "/html/body/div[2]/div/div[6]/button[3]").click()
    time.sleep(1)

    try:
        WebDriverWait(driver,1200).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div/div[6]/button[1]')))
        print("Upload Berhasil")
        time.sleep(1)
        driver.find_element(By.CLASS_NAME, "swal2-actions").click()

    except TimeoutException:
        print("gak berhasil")
        pass
    
    time.sleep(2)
    docid = driver.find_element(By.XPATH, '/html/body/section[2]/section[3]/div/div[1]/div/div[1]/table/tbody/tr[1]/td[2]').text
    docname = driver.find_element(By.XPATH, '/html/body/section[2]/section[3]/div/div[1]/div/div[1]/table/tbody/tr[2]/td[2]').text
    doctime = driver.find_element(By.XPATH, '/html/body/section[2]/section[3]/div/div[1]/div/div[1]/table/tbody/tr[5]/td[2]').text
    print(docid,docname,doctime)

    time.sleep(2)
    driver.find_element(By.XPATH, '/html/body/section[2]/section[3]/ul/li[2]/a').send_keys(Keys.ENTER)
    time.sleep(1)
    driver.find_element(By.XPATH, '/html/body/section[2]/section[3]/ul/li[3]/a').send_keys(Keys.ENTER)
    time.sleep(1)
    driver.find_element(By.XPATH, '/html/body/section[2]/section[3]/ul/li[4]/a').send_keys(Keys.ENTER)

def GetDocID(driver):
    time.sleep(1)
    driver.find_element(By.XPATH, '/html/body/section[2]/section[3]/ul/li[1]/a').send_keys(Keys.ENTER)
    time.sleep(1)
    ambildocid = driver.find_element(By.XPATH, '/html/body/section[2]/section[3]/div/div[1]/div/div[1]/table/tbody/tr[1]/td[2]').text
    return ambildocid
